package lab3

abstract class Animal {
  val hasTail: Boolean
  val numberOfLegs: Int
}

case class Cat(val name: String, val age: Int) extends Animal {
  override val hasTail = true
  override val numberOfLegs = 4
  def favBrandIs(name: String): Boolean = name=="Purina"
}

case class Dog(val name: String, val age: Int, breed: String) extends Animal {
  override val hasTail = true
  override val numberOfLegs = 4
}

