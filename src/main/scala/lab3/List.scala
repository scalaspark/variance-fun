package lab3
abstract class List[+T] {
  def head: T  // return first element (head) of list
  def tail: List[T] // return all but the first element (i.e., the tail) of the list
  def init: List[T] //
  def last: T // return the last element of the list
  def isEmpty: Boolean // the usual
  def size: Int // the usual
  def including[R >: T](x: R): List[R] // Return copy of this list with element prepended
  def excluding[R >: T](x: R): List[R] // return copy of this list with all occurrences of x removed
  def appended[R >: T](x: R): List[R] // Return copy of this list with element x appended
  def filter(p: T => Boolean): List[T] // select all elements of this list satisfying the given predicate
  def map[U](f: T => U): List[U] // the usual
  def drop(n: Int): List[T] // drop (remove) the first n elements of this list, taking the rest
  def take(n: Int): List[T] // take the first n elements of this list, dropping the rest
  def concat[R >: T](xs: List[R]): List[R] // return a list containing this list with that list appended

  // display all elelemnts of this list using the given prefix, separator, and suffix
  def mkString(prefix: String, separator: String, suffix: String): String
  override def toString: String = mkString("[", ", ", "]")
}

// Example of covariance:
// type R = Animal,  type T = Cat  with R >: T
// c = new Cat("Feelix", 50)
// d = new Dog("Charlie", 4, "Golden")
// lc = new Cons[Cat](c, Empty)  // res: a list of cats containing one cat named Felix
// lc.including(d)               // res: a list of animals with a cat, Feelix, and a dog, Charlie

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  override def init: List[T] = if (tail.isEmpty) Empty else tail.init.including(head)
  override def last: T = if (tail.isEmpty) head else tail.last
  override def isEmpty: Boolean = false
  override def size: Int = 1 + tail.size
  override def including[R >: T](x: R): List[R] = new Cons[R](x, this)
  override def excluding[R >: T](x: R): List[R] = filter(t => !(x==t))
  override def appended[R >: T](x: R): List[R] = new Cons[R](head, tail.appended(x))
  override def filter(p: T => Boolean): List[T] = if (p(head)) new Cons(head, tail.filter(p)) else tail.filter(p)
  override def map[U](f: T => U): List[U] = new Cons(f(head), tail.map(f))
  override def drop(n: Int): List[T] = if (n==0) this else tail.drop(n-1)
  override def take(n: Int): List[T] = if (n==0) Empty else new Cons(head, tail.take(n-1))
  override def concat[R >: T](xs: List[R]): List[R] = new Cons(head, tail.concat(xs))
  override def mkString(prefix: String, separator: String, suffix: String): String =
    tail.mkString(prefix + head.toString + separator, separator, suffix)
}

object Empty extends List[Nothing] {
  override def head: Nothing = throw new NoSuchElementException
  override def tail: List[Nothing] = throw new NoSuchElementException
  override def init: List[Nothing] = throw new IllegalArgumentException
  override def last: Nothing = throw new NoSuchElementException
  override def isEmpty: Boolean = true
  override def size: Int = 0
  override def including[R >: Nothing](x: R): List[R] = new Cons(x, Empty)
  override def excluding[R >: Nothing](x: R): List[R] = Empty
  override def appended[R >: Nothing](x: R): List[R] = new Cons(x, Empty)
  override def filter(p: Nothing => Boolean): List[Nothing] = Empty
  override def map[U](f: Nothing => U): List[U] = Empty
  override def drop(n: Int): List[Nothing] = Empty
  override def take(n: Int): List[Nothing] = if (n==0) Empty else throw new NoSuchElementException
  override def concat[R >: Nothing](xs: List[R]): List[R] = xs
  override def mkString(prefix: String, separator: String, suffix: String): String = prefix + suffix
}