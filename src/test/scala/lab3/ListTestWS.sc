import lab3.{Cons, Empty}

val l5 = new Cons(head = 5, Empty)
val l34 = new Cons(3, new Cons(4, Empty))
val l012 = new Cons(0, new Cons(1, new Cons(2, Empty)))
val e = Empty

l012.concat(l34) // [0, 1, 2, 3, 4, ]
l34.filter(_ > 0) // [3, 4, ]
l012.filter(_ < 2) // [0, 1, ]
l012.filter(_ == 5) // []
l5.filter(_ == 5) // [5, ]
l012.map(_ * 2) // [0, 2, 4, ]
l012.appended(3) // [0, 1, 2, 3, ]
l34.including(2) // [2, 3, 4, ]
l012.excluding(1)
l012.init  // [0, 1, ]

// take tests
l5.take(1) // [5, ]
l012.take(0) // []
l012.take(1) // [0, ]
l012.take(2) // [0, 1, ]
l012.take(3) // [0, 1, 2, ]

// drop tests
l012.drop(0) // [0, 1, 2, ]
l012.drop(1) // [1, 2, ]
l012.drop(2) // [2, ]
l012.drop(3) // []
l012.drop(4) // []
// last tests
l012.last // 2
l34.last // 4
l5.last // 5

e.size==0 // true
assert(e.size == 0)
assert(l5.size == 1)
assert(l012.size == 3)


