package lab3

import org.scalatest.funsuite.AnyFunSuite

class ListTestSuite extends AnyFunSuite {
  val l5 = new Cons(head = 5, Empty)
  val l34 = new Cons(3, new Cons(4, Empty))
  val l012 = new Cons(0, new Cons(1, new Cons(2, Empty)))
  val e = Empty

  test("testConcat") {
    assert(l012.concat(l34).toString == "[0, 1, 2, 3, 4, ]")
  }

  test("testFilter") {
    assert(l34.filter(_ > 0).toString == "[3, 4, ]")
    assert(l012.filter(_ < 2).toString == "[0, 1, ]")
    assert(l012.filter(_ == 5) == Empty) // []
    assert(l5.filter(_ == 5).toString == "[5, ]")
  }

  test("testSize") {
    assert(e.size == 0)
    assert(l5.size == 1)
    assert(l012.size == 3)
  }

  test("testAppended") {
    assert(l012.appended(3).toString == "[0, 1, 2, 3, ]")
  }

  test("testExcluding") {
    assert(l012.excluding(1).toString == "[0, 2, ]")
  }

  test("testTake") {
    assert(l5.take(1).toString == "[5, ]")
    assert(l012.take(0) == Empty)
    assert(l012.take(1).toString == "[0, ]")
    assert(l012.take(2).toString == "[0, 1, ]")
    assert(l012.take(3).toString == "[0, 1, 2, ]")
  }

  test("testIncluding") {
    assert(l34.including(2).toString == "[2, 3, 4, ]")
  }

  test("testDrop") {
    assert(l012.drop(0).toString == "[0, 1, 2, ]")
    assert(l012.drop(1).toString == "[1, 2, ]")
    assert(l012.drop(2).toString == "[2, ]")
    assert(l012.drop(3) == Empty)
    assert(l012.drop(4) == Empty)
  }

  test("testLast") {
    assert(l012.last == 2)
    assert(l34.last == 4)
    assert(l5.last == 5)
  }

  test("testMap") {
    assert(l012.map(_ * 2).toString == "[0, 2, 4, ]")
  }

  test("testIsEmpty") {
    assert(e.isEmpty)
    assert(!l5.isEmpty)
  }

  test("testInit") {
    assert(l012.init.toString == "[0, 1, ]")
  }

  test("testHead") {
    assert(l012.head == 0)
    assert(l5.head == 5)
  }

  test("testTail") {
    assert(l012.tail.toString == "[1, 2, ]")
  }


}
