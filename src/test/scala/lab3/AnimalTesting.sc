import lab3.{List, Cons, Empty, Animal, Dog, Cat}

val d1 = new Dog("Charlie", 6, "Golden doodle")
val d2 = new Dog("Chili", 7, "Golden doodle")
val c1 = new Cat(name = "Felix", age = 68)
val l1 = new Cons[Animal](d1, Empty)
val lc = new Cons[Cat](c1, Empty)
l1.including(d2)
l1.appended(d2)
lc.including(d1)
//l1.appended(c1)
//l1.including(c1).including(d2).excluding(d1)

